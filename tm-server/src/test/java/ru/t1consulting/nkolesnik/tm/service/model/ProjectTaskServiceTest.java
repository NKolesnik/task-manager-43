package ru.t1consulting.nkolesnik.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1consulting.nkolesnik.tm.api.service.IConnectionService;
import ru.t1consulting.nkolesnik.tm.api.service.IProjectTaskService;
import ru.t1consulting.nkolesnik.tm.api.service.IPropertyService;
import ru.t1consulting.nkolesnik.tm.api.service.model.IProjectService;
import ru.t1consulting.nkolesnik.tm.api.service.model.ITaskService;
import ru.t1consulting.nkolesnik.tm.api.service.model.IUserService;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.exception.field.ProjectIdEmptyException;
import ru.t1consulting.nkolesnik.tm.exception.field.TaskIdEmptyException;
import ru.t1consulting.nkolesnik.tm.exception.field.UserIdEmptyException;
import ru.t1consulting.nkolesnik.tm.marker.DataCategory;
import ru.t1consulting.nkolesnik.tm.model.Project;
import ru.t1consulting.nkolesnik.tm.model.Task;
import ru.t1consulting.nkolesnik.tm.model.User;
import ru.t1consulting.nkolesnik.tm.service.ConnectionService;
import ru.t1consulting.nkolesnik.tm.service.PropertyService;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.UUID;

@Category(DataCategory.class)
public class ProjectTaskServiceTest {

    @NotNull
    private static final String TASK_NAME_PREFIX = "TEST_TASK_NAME";

    @NotNull
    private static final String TASK_DESCRIPTION_PREFIX = "TEST_TASK_DESCRIPTION";

    @NotNull
    private static final String PROJECT_NAME_PREFIX = "TEST_PROJECT_NAME";

    @NotNull
    private static final String PROJECT_DESCRIPTION_PREFIX = "TEST_PROJECT_DESCRIPTION";

    @NotNull
    private static final String USER_LOGIN = "test";

    @Nullable
    private static final String NULL_USER_ID = null;

    @Nullable
    private static final String NULL_TASK_ID = null;

    @Nullable
    private static final String NULL_PROJECT_ID = null;
    @NotNull
    private static final IPropertyService propertyService = new PropertyService();
    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);
    @NotNull
    private static final ITaskService taskService = new TaskService(connectionService);
    @NotNull
    private static String userId = "";
    @NotNull
    private static String projectId = "";
    @NotNull
    private static String taskId = UUID.randomUUID().toString();
    @NotNull
    private static final IProjectService projectService = new ProjectService(connectionService);
    @NotNull
    private static final IProjectTaskService projectTaskService = new ProjectTaskService(projectService, taskService);
    @NotNull
    private static final IUserService userService = new UserService(connectionService, propertyService);

    @Nullable
    private static Task task;

    @Nullable
    private static User user;

    @Nullable
    private static Project project;

    @BeforeClass
    public static void prepareTestEnvironment() {
        getTestData();
        saveDbBackup();
    }

    @AfterClass
    public static void restoreProdEnvironment() {
        loadDbBackup();
    }

    @SneakyThrows
    public static void saveDbBackup() {
        @NotNull final String userName = propertyService.getDatabaseUsername();
        @NotNull final String userPassword = propertyService.getDatabasePassword();
        @NotNull final String url = propertyService.getDatabaseConnectionString();
        @NotNull final Connection connection = DriverManager.getConnection(url, userName, userPassword);
        connection.setAutoCommit(false);
        @NotNull final String sqlSaveProjectData = "SELECT * INTO \"backup_projects\" FROM \"projects\";";
        @NotNull final String sqlClearProjectTable = "DELETE FROM \"projects\";";
        @NotNull final String sqlSaveTaskData = "SELECT * INTO \"backup_tasks\" FROM \"tasks\";";
        @NotNull final String sqlClearTaskTable = "DELETE FROM \"tasks\";";
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(sqlSaveProjectData);
            statement.executeUpdate(sqlSaveTaskData);
            statement.executeUpdate(sqlClearTaskTable);
            statement.executeUpdate(sqlClearProjectTable);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    public static void loadDbBackup() {
        projectService.removeById(userId, project.getId());
        @NotNull final String userName = propertyService.getDatabaseUsername();
        @NotNull final String userPassword = propertyService.getDatabasePassword();
        @NotNull final String url = propertyService.getDatabaseConnectionString();
        @NotNull final Connection connection = DriverManager.getConnection(url, userName, userPassword);
        connection.setAutoCommit(false);
        @NotNull final String sqlClearProjectTable = "DELETE FROM \"projects\";";
        @NotNull final String sqlLoadProjectData = "INSERT INTO \"projects\" (SELECT * FROM \"backup_projects\");";
        @NotNull final String sqlDropBackupProjectTable = "DROP TABLE \"backup_projects\";";
        @NotNull final String sqlClearTaskTable = "DELETE FROM \"tasks\";";
        @NotNull final String sqlLoadTaskData = "INSERT INTO \"tasks\" (SELECT * FROM \"backup_tasks\");";
        @NotNull final String sqlDropBackupTaskTable = "DROP TABLE \"backup_tasks\";";
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(sqlClearTaskTable);
            statement.executeUpdate(sqlClearProjectTable);
            statement.executeUpdate(sqlLoadProjectData);
            statement.executeUpdate(sqlLoadTaskData);
            statement.executeUpdate(sqlDropBackupTaskTable);
            statement.executeUpdate(sqlDropBackupProjectTable);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    public static void getTestData() {
        user = userService.findByLogin(USER_LOGIN);
        if(user == null){
            user = userService.create("TEST TEST", "1234", Role.USUAL);
        }
        userId = user.getId();
        if(project == null) {
            project = projectService.create(user.getId(), "TEST PROJ");
            projectId = project.getId();
        }
    }

    @NotNull
    private static Task createOneTask() {
        @NotNull final Task task = new Task();
        task.setId(taskId);
        task.setName(TASK_NAME_PREFIX);
        task.setDescription(TASK_DESCRIPTION_PREFIX);
        task.setStatus(Status.IN_PROGRESS);
        task.setUser(user);
        return task;
    }

    @Before
    public void setup() {
        task = createOneTask();
        project = createOneProject();
    }

    @After
    public void cleanup() {
        taskService.clear();
        projectService.clear();
    }

    @Test
    public void bindTaskToProject() {
        projectService.add(userId, project);
        taskService.add(userId, task);
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(NULL_USER_ID, taskId, projectId)
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(userId, projectId, NULL_TASK_ID)
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(userId, NULL_PROJECT_ID, taskId)
        );
        projectTaskService.bindTaskToProject(userId, projectId, task.getId());
        @Nullable final Task repositoryTask = taskService.findById(taskId);
        Assert.assertNotNull(repositoryTask);
        Assert.assertNotNull(repositoryTask.getProject());
        Assert.assertEquals(projectId, repositoryTask.getProject().getId());
    }

    @Test
    public void unbindTaskToProject() {
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> projectTaskService.unbindTaskFromProject(NULL_USER_ID, taskId, projectId)
        );
        Assert.assertThrows(
                TaskIdEmptyException.class,
                () -> projectTaskService.unbindTaskFromProject(userId, projectId, NULL_TASK_ID)
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectTaskService.unbindTaskFromProject(userId, NULL_PROJECT_ID, taskId)
        );
        projectService.add(userId, project);
        taskService.add(userId, task);
        projectTaskService.bindTaskToProject(userId, projectId, taskId);
        @Nullable Task repositoryTask = taskService.findById(taskId);
        Assert.assertNotNull(repositoryTask);
        Assert.assertNotNull(repositoryTask.getProject());
        Assert.assertEquals(projectId, repositoryTask.getProject().getId());
        projectTaskService.unbindTaskFromProject(userId, projectId, taskId);
        repositoryTask = taskService.findById(taskId);
        Assert.assertNotNull(repositoryTask);
        Assert.assertNull(repositoryTask.getProject());
    }

    @NotNull
    private Project createOneProject() {
        @NotNull final Project project = new Project();
        project.setId(projectId);
        project.setName(PROJECT_NAME_PREFIX);
        project.setDescription(PROJECT_DESCRIPTION_PREFIX);
        project.setStatus(Status.IN_PROGRESS);
        project.setUser(user);
        return project;
    }

}
