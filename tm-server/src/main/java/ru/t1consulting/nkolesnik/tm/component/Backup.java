package ru.t1consulting.nkolesnik.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.service.DomainService;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public final class Backup {

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void save() {
        bootstrap.getDomainService().saveDataBackup();
    }

    public void load() {
        if (!Files.exists(Paths.get(DomainService.FILE_BACKUP))) return;
        bootstrap.getDomainService().loadDataBackup();
    }

    public void start() {

    }

    public void stop() {
        executorService.shutdown();
    }

}
