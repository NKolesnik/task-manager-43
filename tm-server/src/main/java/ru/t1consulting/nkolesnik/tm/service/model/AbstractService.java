package ru.t1consulting.nkolesnik.tm.service.model;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.api.repository.model.IRepository;
import ru.t1consulting.nkolesnik.tm.api.service.IConnectionService;
import ru.t1consulting.nkolesnik.tm.api.service.model.IService;
import ru.t1consulting.nkolesnik.tm.model.AbstractModel;

@AllArgsConstructor
public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    protected final IConnectionService connectionService;

}
