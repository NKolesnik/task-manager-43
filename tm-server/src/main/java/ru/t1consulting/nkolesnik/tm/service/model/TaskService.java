package ru.t1consulting.nkolesnik.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.api.repository.model.ITaskRepository;
import ru.t1consulting.nkolesnik.tm.api.service.IConnectionService;
import ru.t1consulting.nkolesnik.tm.api.service.model.ITaskService;
import ru.t1consulting.nkolesnik.tm.enumerated.Sort;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.exception.entity.StatusNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.entity.TaskNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.entity.UserNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.field.*;
import ru.t1consulting.nkolesnik.tm.model.Task;
import ru.t1consulting.nkolesnik.tm.repository.model.TaskRepository;

import javax.persistence.EntityManager;
import java.util.*;

public final class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    public void add(@Nullable final Task task) {
        if (task == null) throw new TaskNotFoundException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @Nullable final ITaskRepository repository = new TaskRepository(em);
            em.getTransaction().begin();
            repository.add(task);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void add(@Nullable final String userId, @Nullable final Task task) {
        if (task == null) throw new TaskNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @Nullable final ITaskRepository repository = new TaskRepository(em);
            em.getTransaction().begin();
            repository.add(userId, task);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void add(@Nullable Collection<Task> tasks) {
        if (tasks == null || tasks.isEmpty()) throw new TaskNotFoundException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @Nullable final ITaskRepository repository = new TaskRepository(em);
            em.getTransaction().begin();
            for (Task task : tasks)
                repository.add(task);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void add(@Nullable final String userId, @Nullable Collection<Task> tasks) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (tasks == null || tasks.isEmpty()) throw new TaskNotFoundException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @Nullable final ITaskRepository repository = new TaskRepository(em);
            em.getTransaction().begin();
            for (Task task : tasks)
                repository.add(userId, task);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void set(@Nullable Collection<Task> tasks) {
        if (tasks == null || tasks.isEmpty()) throw new TaskNotFoundException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @Nullable final ITaskRepository repository = new TaskRepository(em);
            em.getTransaction().begin();
            repository.clear();
            for (Task task : tasks)
                repository.add(task);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void set(@Nullable final String userId, @Nullable Collection<Task> tasks) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (tasks == null || tasks.isEmpty()) throw new TaskNotFoundException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @Nullable final ITaskRepository repository = new TaskRepository(em);
            em.getTransaction().begin();
            repository.clear();
            for (Task task : tasks)
                repository.add(userId, task);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task create(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        @NotNull final Task task = new Task();
        try {
            @Nullable final ITaskRepository repository = new TaskRepository(em);
            em.getTransaction().begin();
            task.setName(name);
            repository.add(userId, task);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        @NotNull final Task task = new Task();
        try {
            @Nullable final ITaskRepository repository = new TaskRepository(em);
            em.getTransaction().begin();
            task.setName(name);
            task.setDescription(description);
            repository.add(userId, task);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        @NotNull final Task task = new Task();
        try {
            @Nullable final ITaskRepository repository = new TaskRepository(em);
            em.getTransaction().begin();
            task.setName(name);
            task.setDescription(description);
            task.setDateBegin(dateBegin);
            task.setDateEnd(dateEnd);
            repository.add(userId, task);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
        return task;
    }

    @Override
    public long getSize() {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @Nullable final ITaskRepository repository = new TaskRepository(em);
            return repository.getSize();
        } finally {
            em.close();
        }
    }

    @Override
    public long getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @Nullable final ITaskRepository repository = new TaskRepository(em);
            return repository.getSize(userId);
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        @Nullable final List<Task> tasks;
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @Nullable final ITaskRepository repository = new TaskRepository(em);
            tasks = repository.findAll();
            if (tasks.isEmpty()) return Collections.emptyList();
            return tasks;
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final List<Task> tasks;
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @Nullable final ITaskRepository repository = new TaskRepository(em);
            tasks = repository.findAll(userId);
            if (tasks.isEmpty()) return Collections.emptyList();
            return tasks;
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable Comparator<Task> comparator) {
        if (comparator == null) return findAll();
        @Nullable final List<Task> taskList;
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @Nullable final ITaskRepository repository = new TaskRepository(em);
            taskList = repository.findAll(comparator);
            if (taskList.isEmpty()) return Collections.emptyList();
            return taskList;
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable Sort sort) {
        if (sort == null) return findAll();
        @Nullable final List<Task> taskList;
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @Nullable final ITaskRepository repository = new TaskRepository(em);
            taskList = repository.findAll(sort);
            if (taskList.isEmpty()) return Collections.emptyList();
            return taskList;
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String userId, @Nullable Comparator<Task> comparator) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll();
        @Nullable final List<Task> taskList;
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @Nullable final ITaskRepository repository = new TaskRepository(em);
            taskList = repository.findAll(userId, comparator);
            if (taskList.isEmpty()) return Collections.emptyList();
            return taskList;
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String userId, @Nullable Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        @Nullable final List<Task> taskList;
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @Nullable final ITaskRepository repository = new TaskRepository(em);
            taskList = repository.findAll(userId, sort);
            if (taskList.isEmpty()) return Collections.emptyList();
            return taskList;
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        if (projectId == null || projectId.isEmpty()) return findAll(userId);
        @Nullable List<Task> result;
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @Nullable final ITaskRepository repository = new TaskRepository(em);
            result = repository.findAllByProjectId(userId, projectId);
            if (result.isEmpty()) return Collections.emptyList();
            return result;
        } finally {
            em.close();
        }
    }

    @Nullable
    @Override
    public Task findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @Nullable final ITaskRepository repository = new TaskRepository(em);
            return repository.findById(id);
        } finally {
            em.close();
        }
    }

    @Nullable
    @Override
    public Task findById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @Nullable final ITaskRepository repository = new TaskRepository(em);
            return repository.findById(userId, id);
        } finally {
            em.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @Nullable final ITaskRepository repository = new TaskRepository(em);
            return repository.existsById(id);
        } finally {
            em.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        if (userId == null || userId.isEmpty()) return false;
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @Nullable final ITaskRepository repository = new TaskRepository(em);
            return repository.existsById(userId, id);
        } finally {
            em.close();
        }
    }

    @Override
    public void update(@Nullable final Task task) {
        if (task == null) throw new TaskNotFoundException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @Nullable final ITaskRepository repository = new TaskRepository(em);
            em.getTransaction().begin();
            repository.update(task);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @Nullable final ITaskRepository repository = new TaskRepository(em);
            em.getTransaction().begin();
            @Nullable final Task task = repository.findById(userId, id);
            if (task == null) throw new TaskNotFoundException();
            task.setName(name);
            task.setDescription(description);
            repository.update(task);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public void changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (status == null) throw new StatusNotFoundException();
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @Nullable final ITaskRepository repository = new TaskRepository(em);
            em.getTransaction().begin();
            @Nullable final Task task = repository.findById(userId, id);
            if (task == null) throw new TaskNotFoundException();
            task.setStatus(status);
            repository.update(task);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void clear() {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @Nullable final ITaskRepository repository = new TaskRepository(em);
            em.getTransaction().begin();
            repository.clear();
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @Nullable final ITaskRepository repository = new TaskRepository(em);
            em.getTransaction().begin();
            repository.clear(userId);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void remove(@Nullable final Task task) {
        if (task == null) throw new TaskNotFoundException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @Nullable final ITaskRepository repository = new TaskRepository(em);
            em.getTransaction().begin();
            @NotNull final String id = task.getId();
            repository.removeById(id);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final Task task) {
        if (task == null) throw new TaskNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @Nullable final ITaskRepository repository = new TaskRepository(em);
            em.getTransaction().begin();
            @NotNull final String id = task.getId();
            repository.removeById(userId, id);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @Nullable final ITaskRepository repository = new TaskRepository(em);
            em.getTransaction().begin();
            repository.removeById(id);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @Nullable final ITaskRepository repository = new TaskRepository(em);
            em.getTransaction().begin();
            repository.removeById(userId, id);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void removeByProjectId(@Nullable String userId, @Nullable String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            @Nullable final ITaskRepository repository = new TaskRepository(em);
            em.getTransaction().begin();
            repository.removeByProjectId(userId, projectId);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

}
