package ru.t1consulting.nkolesnik.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1consulting.nkolesnik.tm.api.model.IWBS;

import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "projects")
public final class ProjectDTO extends AbstractWbsModelDTO implements IWBS {

    private static final long serialVersionUID = 1;

}
