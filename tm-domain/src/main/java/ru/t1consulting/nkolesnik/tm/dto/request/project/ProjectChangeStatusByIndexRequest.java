package ru.t1consulting.nkolesnik.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.dto.request.AbstractUserRequest;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public class ProjectChangeStatusByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    @Nullable
    private Status status;

    public ProjectChangeStatusByIndexRequest(@Nullable String token) {
        super(token);
    }

    public ProjectChangeStatusByIndexRequest(@Nullable String token, @Nullable Integer index, @Nullable Status status) {
        super(token);
        this.index = index;
        this.status = status;
    }

}
